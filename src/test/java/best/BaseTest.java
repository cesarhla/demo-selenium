package best;

import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    protected WebDriver driver;

    static{
        System.setProperty("webdriver.chrome.driver", "/home/albuquerque/Downloads/chromedriver");
    }

    public BaseTest(WebDriver driver){

        this.driver=driver;
    }


    public void tearDown(){
        driver.quit();
    }

    public void setup(){
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);

    }
}
