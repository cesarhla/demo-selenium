package best;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {

    private WebElement passwordInput;

    private WebElement loginInput;

    private WebElement enterbutton;

    private WebDriver driver;

    public LoginPage(WebDriver driver){
        this.driver= driver;
    }

    private WebElement getPasswordInput(){
        return driver.findElement(By.xpath("//input[@id='senha']"));
    }

    private WebElement getLoginInput(){
        return driver.findElement(By.name("login"));
    }

    private WebElement getEnterButton(){
        return driver.findElement(By.className("dialog-button"));
    }

    public void fillPassword(String password){
        this.getPasswordInput().sendKeys(password);
    }

    public void fillLogin(String login){
        this.getLoginInput().sendKeys(login);
    }

    public void clickOnEnterButton(){
        this.getEnterButton().click();
    }

    public void visit(){
        driver.get("http://www.professornilson.com/mobile/app/index.html");
    }

}
