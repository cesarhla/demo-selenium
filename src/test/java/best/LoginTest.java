package best;


import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class LoginTest extends BaseTest{


    private LoginPage loginPage;

    public LoginTest(){
        super( new ChromeDriver());
    }

    @Before
    public void setup(){
        super.setup();
        loginPage = new LoginPage(driver);
    }

    @Test
    public void testLoginSuccess(){
        String password = "";
        String login = "";
        loginPage.visit();
        loginPage.fillLogin(login);
        loginPage.fillPassword(password);
        loginPage.clickOnEnterButton();
    }

}
