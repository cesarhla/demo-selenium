package demo;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class LoginTest {

    @Test
    public void testLogin(){

        System.setProperty("webdriver.chrome.driver", "/home/albuquerque/Downloads/chromedriver");
        WebDriver driver =  new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(120, TimeUnit.SECONDS);

        driver.get("http://www.professornilson.com/mobile/app/index.html");
        WebElement inputLogin = driver.findElement(By.name("login"));
        inputLogin.sendKeys("");

        WebElement inpuPassword = driver.findElement(By.xpath("//input[@id='senha']"));
        inpuPassword.sendKeys("");
        WebElement button = driver.findElement(By.className("dialog-button"));
        button.click();

        WebElement content = driver.findElement(By.xpath("//span[contains(@class,'mdl-layout__tab-ripple-container') and position() = 1]"));
        content.click();
        List<WebElement> news =  driver.findElements(By.xpath("//section//div[@class='shape']"));

        Assert.assertEquals(5, news.size());
        driver.quit();
    }
}
